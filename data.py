import os
from PIL import Image

from path_catalogs import DatasetCatalog as dataset_catalog
from torch.utils.data import Dataset
from torchvision import datasets, transforms
import numpy as np
import torch
import cv2

__all__ = ['MNIST', 'ImageDataset']


def scaling(img, img_size):
    '''
    Args:
         img         : (numpy.array) input image
         channels    : (int) number of input image channels
         img_size    : (int) dimensions of output image(square)
    Returns:
         Scaled image
    '''
    img_w, img_h = img.shape[1], img.shape[0]

    # Getting new scales
    new_w = int(img_w * min(img_size/img_h, img_size/img_w))
    new_h = int(img_h * min(img_size/img_h, img_size/img_w))

    # Getting paddings
    pad_w = (img_size - new_w)//2
    pad_h = (img_size - new_h)//2

    # Resize
    img = cv2.resize(img, (new_w, new_h), interpolation=cv2.INTER_CUBIC)

    # Padding
    img = np.pad(img, ((0, 0), (pad_h, pad_h), (pad_w, pad_w)), 'constant', constant_values=128)

    return img


# MNIST dataset definition
class MNIST(Dataset):
    def __init__(self, root, transforms=None):
        '''
        Args:
             folder_path    : (string) directory storing MNIST data set
             img_size       : (int) input dimensions of MNIST images
        '''
        super(MNIST, self).__init__()
        self.root = root
        self.img_list = os.listdir(root)
        self._transforms = transforms

    def __getitem__(self, idx):
        '''
        Args:
             idx            : (int) required index of corresponding data
        Returns:
             Required image (tensor)
        '''
        img = Image.open(os.path.join(self.root, self.img_list[idx])).convert('RGB')
        # img = np.array(img)

        if self._transforms:
            img = self._transforms(img)
        return img

    def __len__(self):
        return len(self.img_list)


class ImageDataset(Dataset):
    def __init__(self, root, transforms=None):
        '''
        :param root (str): Root directory, storing train set images
        :param transforms: Transformations for data
        '''
        super(ImageDataset, self).__init__()
        self.root = root

        # Find images contained in root directory
        self.img_list = os.listdir(root)

        self._transforms = transforms

    def __getitem__(self, idx):
        '''
        :param idx: Id of image wish to yield from dataset
        :return:
        '''
        img = Image.open(os.path.join(self.root, self.img_list[idx])).convert('RGB')
        # img = np.array(img)

        if self._transforms:
            img = self._transforms(img)
        return img

    def __len__(self):
        return len(self.img_list)


