# Copyright (c) Shaoshu Yang, 2020
__all__ = ['GAN']

import torch.nn as nn


def make_deconv(
        in_dim,
        out_dim,
        kernel_size,
        stride=2,
        padding=1,
        norm=True):
    """
        Deconvolution layer, used as upsamplie layer in our model instead of
        interpolation upsample.
    """
    if norm:
        return nn.Sequential(
            nn.ConvTranspose2d(
                in_dim,
                out_dim,
                kernel_size,
                stride=stride,
                padding=padding,
                bias=False),
            nn.BatchNorm2d(out_dim),
            nn.ReLU()
        )
    else:
        return nn.Sequential(
            nn.ConvTranspose2d(
                in_dim,
                out_dim,
                kernel_size,
                stride=stride,
                padding=padding,
                bias=True),
            nn.ReLU()
        )


def make_conv(
        in_dim,
        out_dim,
        kernel_size=4,
        stride=2,
        padding=1,
        norm=True):
    """
        Convolution 2D layer, with activation funciton.
    """
    if norm:
        return nn.Sequential(
            nn.Conv2d(
                in_dim,
                out_dim,
                kernel_size,
                stride=stride,
                padding=padding,
                bias=True),
            nn.BatchNorm2d(out_dim),
            nn.LeakyReLU(0.2)
        )
    else:
        return nn.Sequential(
            nn.Conv2d(
                in_dim,
                out_dim,
                kernel_size,
                stride=stride,
                padding=padding,
                bias=False),
            nn.LeakyReLU(0.2)
        )


class Generator(nn.Module):

    def __init__(self,
                 input_dim=128,
                 output_channels=3,
                 dim=64,
                 n_upsamplings=4,
                 norm=True):
        super().__init__()

        layers = []

        # 1: 1x1 -> 4x4
        d = min(dim * 2 ** (n_upsamplings - 1), dim * 8)
        layers += [make_deconv(input_dim, d, kernel_size=4, stride=1,
                               padding=0, norm=norm)]

        # 2: upsamplings, 4x4 -> 8x8 -> 16x16 -> 32x32 -> ....
        for i in range(n_upsamplings - 1):
            d_last = d
            d = min(dim * 2 ** (n_upsamplings - 2 - i), dim * 8)
            layers += [make_deconv(d_last, d, kernel_size=4, stride=2,
                                   padding=1, norm=norm)]

        layers += [
            nn.ConvTranspose2d(d, output_channels, kernel_size=4,
                               stride=2, padding=1),
            nn.Tanh()
        ]

        self.net = nn.Sequential(*layers)

    def forward(self, z):
        x = self.net(z)
        return x


class Discriminator(nn.Module):

    def __init__(self,
                 input_channels=3,
                 dim=64,
                 n_downsamplings=4,
                 norm=True):
        super().__init__()

        layers = []

        # 1: downsamplings, ... -> 16x16 -> 8x8 -> 4x4
        d = dim
        layers += [nn.Conv2d(
            input_channels,
            d,
            kernel_size=4,
            stride=2,
            padding=1),
            nn.LeakyReLU(0.2)]

        for i in range(n_downsamplings - 1):
            d_last = d
            d = min(dim * 2 ** (i + 1), dim * 8)
            layers += [make_conv(d_last, d, kernel_size=4, stride=2, padding=1)]

        # 2: logit
        layers += [nn.Conv2d(d, 1, kernel_size=4, stride=1, padding=0)]

        self.net = nn.Sequential(*layers)

    def forward(self, x):
        y = self.net(x)
        return y


# class Generator(nn.Module):
#     def __init__(self, dim_in, dim_out, img_size):
#         '''
#         Args:
#              dim_in       : (int) number of imput channels
#              dim_out      : (int) number of output channels
#              img_size     : (int) output image dimension
#         '''
#         super(Generator, self).__init__()
#         self.dim_in = dim_in
#         self.dim_out = dim_out
#         self.img_size = img_size
#
#         self.fc = nn.Sequential(
#             nn.Linear(self.dim_in, 1024),
#             nn.BatchNorm1d(1024),
#             nn.ReLU(),
#             nn.Linear(1024, 128*(self.img_size//4)*(self.img_size//4)),
#             nn.BatchNorm1d(128*(self.img_size//4)*(self.img_size//4)),
#             nn.ReLU()
#         )
#         self.deconv = nn.Sequential(
#             nn.ConvTranspose2d(128, 64, 4, 2, 1),
#             nn.BatchNorm2d(64),
#             nn.ReLU(),
#             nn.ConvTranspose2d(64, self.dim_out, 4, 2, 1),
#             nn.Tanh()
#         )
#
#     def forward(self, x):
#         '''
#         Args:
#              x           : (tensor) input tensor
#         '''
#         x = x.view(-1, 128)
#         x = self.fc(x)
#         x = x.view(-1, 128, self.img_size//4, self.img_size//4)
#         x = self.deconv(x)
#
#         return x
#
#
# class Discriminator(nn.Module):
#     def __init__(self, dim_in, dim_out, img_size):
#         '''
#         Args:
#              dim_in       : (int) number of imput channels
#              dim_out      : (int) number of output channels
#              img_size     : (int) input image dimension
#         '''
#         super(Discriminator, self).__init__()
#         self.dim_in = dim_in
#         self.dim_out = dim_out
#         self.img_size = img_size
#
#         self.conv = nn.Sequential(
#             nn.Conv2d(self.dim_in, 64, 4, 2, 1),
#             nn.LeakyReLU(0.2),
#             nn.Conv2d(64, 128, 4, 2, 1),
#             nn.BatchNorm2d(128),
#             nn.LeakyReLU(0.2)
#         )
#         self.fc = nn.Sequential(
#             nn.Linear(128*(self.img_size//4)*(self.img_size//4), 1024),
#             nn.BatchNorm1d(1024),
#             nn.LeakyReLU(0.2),
#             nn.Linear(1024, self.dim_out),
#         )
#
#     def forward(self, x):
#         x = self.conv(x)
#         x = x.view(-1, 128*(self.img_size//4)*(self.img_size//4))
#         x = self.fc(x)
#
#         return x


def GAN(cfg):
    '''
    :param cfg: Configuration file
    :return: Generator and Discriminator
    '''
    # Acquire model filter size definition
    in_dim = cfg.MODEL.G_IN_CHANNEL
    out_channel = cfg.MODEL.G_OUT_CHANNEL
    midium_dim = cfg.MODEL.MIDIUM_DIM
    img_size = cfg.INPUT.SIZE_TRAIN

    # Down sample times
    n_downsample = cfg.MODEL.N_DOWNSAMPLE
    # Use batch norm
    norm = cfg.MODEL.NORM

    # Create Generator and Discriminator
    G = Generator(
        input_dim=in_dim,
        output_channels=out_channel,
        dim=midium_dim,
        n_upsamplings=n_downsample,
        norm=norm
    )

    D = Discriminator(
        input_channels=out_channel,
        dim=midium_dim,
        n_downsamplings=n_downsample,
        norm=norm
    )
    # G = Generator(
    #     dim_in=in_dim,
    #     dim_out=out_channel,
    #     img_size=img_size
    # )
    #
    # D = Discriminator(
    #     dim_in=out_channel,
    #     dim_out=1,
    #     img_size=img_size
    # )

    return G, D




