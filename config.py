# Copyright (c) Shaoshu Yang, 2020
import os
from yacs.config import CfgNode as CN

_C = CN()

# -----------------------------------------------------------------------------
# Config definition
# -----------------------------------------------------------------------------
# Model configurations

_C.MODEL = CN()
_C.MODEL.DEVICE = "cuda"
_C.MODEL.USE_CUDNN = True

_C.MODEL.WEIGHT = ""

# Use batch normalize
_C.MODEL.NORM = True

# Filter dimension factor
_C.MODEL.MIDIUM_DIM = 64
_C.MODEL.G_IN_CHANNEL = 128
_C.MODEL.G_OUT_CHANNEL = 3

# Up sample times for generator/down sample times for discriminator
_C.MODEL.N_DOWNSAMPLE = 4

# ---------------------------------------------------------------------------- #
# Solver
# ---------------------------------------------------------------------------- #
_C.SOLVER = CN()
_C.SOLVER.MAX_EPOCH = 200

# Coefficient for gradient penalty
_C.SOLVER.LAMBDA = 0.25
_C.SOLVER.K = 1

_C.SOLVER.CHECKPOINT_PERIOD = 2

# Number of images per batch
# This is global, so if we have 8 GPUs and IMS_PER_BATCH = 16, each GPU will
# see 2 images per batch
_C.SOLVER.IMS_PER_BATCH = 1

# Test sampling amount
_C.SOLVER.SAMPLE_AMOUNT = 100
_C.SOLVER.SAMPLE_INTERVAL = 1

# Adam optimizer configurations
_C.SOLVER.BETA1 = 0.5
_C.SOLVER.BETA2 = 0.999

# Base learn rate for discriminator and generator
_C.SOLVER.BASE_LR_G = 1e-4
_C.SOLVER.BASE_LR_D = 1e-4

_C.SOLVER.N_D = 1

# -----------------------------------------------------------------------------
# DataLoader
# -----------------------------------------------------------------------------
_C.DATALOADER = CN()
# Number of data loading threads
_C.DATALOADER.NUM_WORKERS = 4

# If > 0, this enforces that each collated batch should have a size divisible
# by SIZE_DIVISIBILITY
_C.DATALOADER.SIZE_DIVISIBILITY = 0
_C.DATALOADER.DROP_LAST = False
_C.DATALOADER.SHUFFLE = True

# Transfer a batch to CUDA pin memory before returning
_C.DATALOADER.PIN_MEMORY = False


# -----------------------------------------------------------------------------
# INPUT
# -----------------------------------------------------------------------------
_C.INPUT = CN()
# Size of the smallest side of the image during training
_C.INPUT.SIZE_TRAIN = 128  # (800,)


# -----------------------------------------------------------------------------
# Dataset
# -----------------------------------------------------------------------------
_C.DATASETS = CN()
# List of the dataset names for training, as present in paths_catalog.py
_C.DATASETS.TRAIN = ()

# -----------------------------------------------------------------------------
# INPUT
# -----------------------------------------------------------------------------
_C.INPUT = CN()
# Size the image during training
_C.INPUT.SIZE_TRAIN = 64  # (800,)


# -----------------------------------------------------------------------------
# Logging and checkpointer
# -----------------------------------------------------------------------------
_C.OUTPUT_DIR = "output/"