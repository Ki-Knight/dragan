# Copyright (c) Shaoshu Yang, 2020
import os
import cv2
import torch
import argparse
import numpy as np
import torch.optim as optim
import torch.nn as nn
import torch.backends.cudnn as cudnn
from torch.utils.data import DataLoader
from torch.autograd import grad

from build import build_dataset
from utils import Checkpointer, setup_logger
from dragan import GAN
from config import _C as cfg


def train(cfg):
    batch_size = cfg.SOLVER.IMS_PER_BATCH
    z_dim = cfg.MODEL.G_IN_CHANNEL

    # Test set
    z_sample = torch.normal(0.5, 0.5, (cfg.SOLVER.SAMPLE_AMOUNT, z_dim, 1, 1))

    # Set training hyper parameters
    lambda_ = cfg.SOLVER.LAMBDA
    k = cfg.SOLVER.K
    n_d = cfg.SOLVER.N_D

    max_epoch = cfg.SOLVER.MAX_EPOCH
    sample_interval = cfg.SOLVER.SAMPLE_INTERVAL
    checkpoint_interval = cfg.SOLVER.CHECKPOINT_PERIOD

    use_cuda = True if cfg.MODEL.DEVICE == "cuda" else False

    # Build DRAGAN
    G, D = GAN(cfg)

    # Build dataset
    dataset = build_dataset(cfg)
    data_loader = DataLoader(
        dataset=dataset,
        batch_size=batch_size,
        shuffle=cfg.DATALOADER.SHUFFLE,
        num_workers=cfg.DATALOADER.NUM_WORKERS,
        drop_last=cfg.DATALOADER.DROP_LAST,
        pin_memory=cfg.DATALOADER.PIN_MEMORY,
    )

    # x = dataset[1]
    # Set logger
    output_dir = cfg.OUTPUT_DIR
    logger = setup_logger("dragan", output_dir)
    # logger.info("Using {} GPUs".format(num_gpus))
    logger.info(cfg)

    logger.info("Loaded configuration file {}".format(args.config_file))

    # Set optimizer
    G_optimizer = optim.Adam(G.parameters(), lr=cfg.SOLVER.BASE_LR_G, betas=(cfg.SOLVER.BETA1, cfg.SOLVER.BETA2))
    D_optimizer = optim.Adam(D.parameters(), lr=cfg.SOLVER.BASE_LR_D, betas=(cfg.SOLVER.BETA1, cfg.SOLVER.BETA2))

    # Set loss function
    BCEloss = torch.nn.BCEWithLogitsLoss()

    # Label for disciminator
    y_real, y_fake = torch.ones(batch_size, 1, 1, 1), torch.zeros(batch_size, 1, 1, 1)
    # y_real, y_fake = torch.ones(batch_size, 1), torch.zeros(batch_size, 1)
    if use_cuda:
        BCEloss.cuda()
        y_real, y_fake = y_real.cuda(), y_fake.cuda()
        G.cuda()
        D.cuda()
        z_sample = z_sample.cuda()
    if cfg.MODEL.USE_CUDNN:
        cudnn.benchmark = True

    # Set checkpointer
    arguments = dict()
    arguments["epoch"] = 0

    checkpointer = Checkpointer(G, D, G_optimizer, D_optimizer, save_dir=output_dir, save_to_disk=True)
    checkpoint = cfg.MODEL.WEIGHT

    if checkpoint != "":
        extra_checkpoint_data = checkpointer.load(checkpoint)
    else:
        extra_checkpoint_data = checkpointer.load()
    arguments.update(extra_checkpoint_data)

    # Training process
    for epoch in range(arguments["epoch"] + 1, max_epoch):
        G.train()
        D.train()
        for i, x in enumerate(data_loader):
            if isinstance(x, list):
                x = x[0]
            # Losses recorder
            arguments["epoch"] = epoch
            losses = []

            # Prepare input
            z = torch.normal(0.5, 0.5, (batch_size, z_dim, 1, 1))
            if use_cuda:
                x = x.cuda()
                z = z.cuda()

            G_optimizer.zero_grad()
            D_optimizer.zero_grad()

            # Loss of discriminator
            d_real = D(x)
            d_real_loss = BCEloss(d_real, y_real)

            x_fake = G(z)
            d_fake = D(x_fake)
            d_fake_loss = BCEloss(d_fake, y_fake)

            # Gradient penalty
            alpha = torch.rand(batch_size, 1, 1, 1)
            if use_cuda:
                x_p = x + 0.5 * x.std() * torch.rand(x.size()).cuda()
            else:
                x_p = x + 0.5 * x.std() * torch.rand(x.size())
            if use_cuda:
                alpha = alpha.cuda()
            difference = x_p - x
            interpolates = x + (alpha*difference)
            interpolates.requires_grad = True
            pred_hat = D(interpolates)
            if use_cuda:
                gradients = grad(outputs=pred_hat, inputs=interpolates, grad_outputs=torch.ones(pred_hat.size()).cuda(),
                                 create_graph=True, retain_graph=True, only_inputs=True)[0]

            else:
                gradients = grad(outputs=pred_hat, inputs=interpolates, grad_outputs=torch.ones(pred_hat.size()),
                                 create_graph=True, retain_graph=True, only_inputs=True)[0]

            gradient_penalty = lambda_*((gradients.view(gradients.size()[0], -1).norm(2, 1) - k)**2).mean()

            # Back propagation for discriminator
            d_loss = d_real_loss + d_fake_loss + gradient_penalty
            d_loss.backward()
            D_optimizer.step()

            # Train generator
            if i % n_d == 0:
                x_fake = G(z)
                d_fake = D(x_fake)
                g_loss = BCEloss(d_fake, y_real)
                g_loss.backward()
                G_optimizer.step()

                # Upgrade logger
                losses.append(d_loss.item())
                losses.append(g_loss.item())
                losses.append(d_loss.item() + g_loss.item())
                logger.info('[Epoch %d/%d, Batch %d/%d] [Losses: G_loss %f, D_loss %f, total %f]'
                            %(epoch, max_epoch, i + 1, len(data_loader),
                           losses[0], losses[1], losses[2]))

        if epoch % sample_interval == 0:
            G.eval()
            visualize_result(G, epoch, z_sample, output_dir)
        if epoch % checkpoint_interval == 0:
            checkpointer.save("model_{:07d}.pth".format(epoch), **arguments)


def visualize_result(G, epoch, z_sample, result_dir):
    len = z_sample.shape[0]
    for i in range(len):
        # Generate sample from generator
        img = G(z_sample[i].unsqueeze(0))

        # Denormalize and save image
        img = img.squeeze(0).cpu().data.numpy() * 255.0
        img = np.array(img, dtype=int).transpose((1, 2, 0))[:, :, ::-1]
        cv2.imwrite(os.path.join(result_dir + "images/", "%d_%05d.jpg" % (epoch, i)), img)


if __name__ == "__main__":
    # Create argument parser, read directory of configuration file
    parser = argparse.ArgumentParser(description="DRAGAN Training Experiment:")
    parser.add_argument("--config_file", default="./configs/conv_dragan_anime.yaml", type=str)
    args = parser.parse_args()

    cfg.merge_from_file(args.config_file)

    train(cfg)
