# Copyright (c) Shaoshu Yang, 2020
from path_catalogs import DatasetCatalog as dataset_catalog
from torchvision import datasets, transforms

import data as D


def build_dataset(cfg):
    """
        Arguments:
            dataset_list (list[str]): Contains the names of the datasets, i.e.,
                coco_2014_train, coco_2014_val, etc
            transforms (callable): transforms to apply to each (image, target) sample
            dataset_catalog (DatasetCatalog): contains the information on how to
                construct a dataset.
            is_train (bool): whether to setup the dataset for training or testing
        """

    dataset_list = cfg.DATASETS.TRAIN
    if not isinstance(dataset_list, (list, tuple)):
        raise RuntimeError(
            "dataset_list should be a list of strings, got {}".format(dataset_list)
        )
    for dataset_name in dataset_list:
        data = dataset_catalog.get(dataset_name)
        factory = getattr(D, data["factory"])
        args = data["args"]
        # for COCODataset, we want to remove images without annotations
        # during training
        resize = cfg.INPUT.SIZE_TRAIN
        if data["factory"] == "MNIST":
            transform = transforms.Compose([
                transforms.Resize(size=(resize, resize)),
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.5], std=[0.5])
            ])
            args["transforms"] = transform
            dataset = datasets.MNIST(args["root"], transform=transform, download=True)
        if data["factory"] == "ImageDataset":
            transform = transforms.Compose([
                transforms.Resize(size=(resize, resize)),
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
            ])
            args["transforms"] = transform
            # make dataset from factory
            dataset = factory(**args)

    return dataset