# Copyright (c) Shaoshu Yang, 2020
import os
from copy import deepcopy


class DatasetCatalog(object):
    DATA_DIR = "./datasets"
    DATASETS = {
        "anime": {
            "img_dir": "faces",
        },
        "mnist": {
            "img_dir": "mnist",
        },
        "test": {
            "img_dir": "test",
        }
    }

    @staticmethod
    def get(name):
        if "anime" in name:
            data_dir = DatasetCatalog.DATA_DIR
            attrs = DatasetCatalog.DATASETS[name]
            args = dict(
                root=os.path.join(data_dir, attrs["img_dir"])
            )
            return dict(
                factory="ImageDataset",
                args=args,
            )
        elif "mnist" or "test" in name:
            data_dir = DatasetCatalog.DATA_DIR
            attrs = DatasetCatalog.DATASETS[name]
            args = dict(
                root=os.path.join(data_dir, attrs["img_dir"])
            )
            return dict(
                factory="MNIST",
                args=args,
            )

        raise RuntimeError("Dataset not available: {}".format(name))